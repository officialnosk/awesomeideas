# Contributing Guideline

All contributions are welcome including :

- Proposing new Ideas
- Giving awesome resources for learning

## Report bugs using Github's [issues](https://gitlab.com/officialnosk/awesomeideas/-/issues/new)

- Create the issue which you found with a quick summary and/or background if you can
- Explain in detail if you can.

_If you want to give idea, ceate a issue and give description of ideas that you want._

## Work on Issue

First of all, this is a collection of ideas.
You're going to need to following things installed to contribute to this project.

0. Git program [link](https://git-scm.com/)
1. Fork the repo. [link](https://gitlab.com/officialnosk/awesomeideas/-/forks/new)
2. Clone your fork to your local machine. `git clone <link after forking>`
3. Create a branch for your fix : `git checkout -b <branch-name-here>`.
4. Copy the sample `sampleidea.md` and rename it to your preference `<your-prefered-name>`.
5. Add appropriate ideas and descriptions according to your ideas.
6. [Optional] Make the appropriate changes for the issue you are trying to address or the feature that you want to add.
7. Use `git add .` to add file contents of changed files.
8. Commit after changes. `git commit -m "meaningful-message"`. Our recommendation use conventional commits [link](https://www.conventionalcommits.org/)
9. Push the changes to the remote repository using `git push origin <your-branch-name-here>`.
10. Submit a pull request to the upstream repository with meaningful description and title.
11. Wait for the merge!


> If you face any problem while setting up project you can contact us using any of the followings:

- [Telegram](https://t.me/noskgroup)
- [Facebook](https://facebook.com/nosklub)
- [Twitter](https://twitter.com/officialnosk)

### Note - Commit & PR Title :

It’s good to have descriptive commit messages so that other folks can make sense of what your commit is doing.
We recommend you to read about [Husky](https://github.com/typicode/husky/blob/master/README.md) to prevent bad `git commit`, `git push` and more 🐶 \_woof!

Read [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) before making the commit message.

### How your idea is going to approved?

Those ideas which are easy to understand, correct in grammar are given more priority. Duplicate ideas will be rejected. We will be commenting in your ideas if we find anything wrong.

## License

By contributing, you agree that your contributions will be licensed under its MIT License.

## References

This document was adapted from the open-source contribution guidelines for [Facebook's Draft](https://github.com/facebook/draft-js/blob/a9316a723f9e918afde44dea68b5f9f39b7d9b00/CONTRIBUTING.md)
