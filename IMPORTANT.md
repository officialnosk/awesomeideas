# Important stuffs we generally forgot to do or we don't care too much.

1. Please use this format while commiting 
    ```
    [OPTIONAL:] <verb> <what you do>
    Example:
    if I changed the spelling of docs.
    fix: correct the spelling of <DOCUMENT NAME>
    ```
2. Check whether you have added the project so that it is now in staging area.
    ```
    git status
    git add <NAME OF FILE/S>
    TO ADD ALL THE FILES AT ONCE
    git add .
    ```
3. Check whether you have commited before pushing
    ```
    git log
    ```
4. If you found the the name you are using to push is different.
    ```
    SET THE LOCAL CONFIG OF YOUR PROJECT
    git config --local user.name "YOUR NAME"
    git config --local user.email "YOUR EMAIL"
    ```
5. In case if you are using the SSH in gitlab and while pushing to gitlab credentials is required.
    ```
    git clone <SSH URL NOT HTTPS>
    or
    CHECK THE ORIGIN
    git remote show origin
    SET THE ORIGIN IF NEEDED
    git remote set-url origin <URL>
    ```
6. While pushing you may not be using your branch instead you may be using main branch or master branch.
    ```
    CHANGE THE HEAD TO YOUR BRANCH
    git remote set-head origin <YOUR BRANCH NAME>
    ```
7. Alternative approach to STEP 6
    ```
    WHILE PUSHING USE BRANCH NAME
    git push --set-upstream origin <BRANCH NAME>

### NOTE: This may not be the complete things we don't consider. You can add here too

## THANK YOU
